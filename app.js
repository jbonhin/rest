const express = require('express');
const app = express();
const morgan = require("morgan"); /* C:\yarn add express morgan */
const bodyParser = require("body-parser"); /* C:\ npm install --save body-parser */

const rotaProdutos = require('./routes/produtos');
const rotaPedidos = require('./routes/pedidos');
const rotaUsuarios = require('./routes/usuarios');

/*
Morgan, que é uma forma de logar ou mostrar quais requisições estão chegando em nosso servidor HTTP,
seja ele feito no Express ou em Node puro utilizando o módulo HTTP.
*/
app.use(morgan('dev'));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: false })); //apenas dados simples
app.use(bodyParser.json()); // json de entrada no body

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Header',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATH, DELETE, GET');
    return res.status(200).send({});
  }
  next();
});
app.use('/produtos', rotaProdutos);
app.use('/pedidos', rotaPedidos);
app.use('/usuarios', rotaUsuarios);

// QUANDO NÃO ENCONTRA A ROTA
app.use((req, res, next) => {
  const erro = new Error('Rota Não Encontrada!!');
  erro.status = 404;
  next(erro);
});

app.use((error, req, res, next) => {
  res.status(error.stastus || 500);
  return res.send({
    erro: {
      mensagem: error.message
    }
  });
});

module.exports = app;
